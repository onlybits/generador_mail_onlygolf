<?php if(isset($_GET['limpio'])): ?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title><?php echo get_taxonomy_data('name', 'tipo_mail'); ?> - <?php echo php::get_data('title'); ?></title>
</head>
<body class="mail-css">
<?php echo get_template_part('content', 'mail'); ?>
</body>
</html>
<?php else: ?>

<?php get_header(); ?>

<?php //if(have_posts()) : while (have_posts()) : the_post(); ?>

<!-- ================================================= CONTENT ================================================= -->
<div class="content">
	<div class="container">
		<!-- MAIN CONTAINER -->
		
		<div class="row">
			<div class="col-12">
				<h2 class="border-bottom pb-3 mb-3">C&oacute;digo Mail</h2>
				<?php if(isset($_GET['codigo'])): ?>
				<p>El siguiente c&oacute;digo se debe copiar en el editor <b>HTML</b> del gestor de correos, ya sea <code>Mailchimp</code> o <code>Fidelizador</code>.</p>
				<?php else: ?>
				<p>Haga click en el siguiente bot&oacute;n para generar el <b>c&oacute;digo</b> para utilizar en <code>Mailchimp</code> o <code>Fidelizador</code>.</p>
				<?php endif; ?>
			</div>
			<?php if(isset($_GET['codigo'])): ?>
			<div class="col-12 mb-2">
				<div class="form-group">
					<?php
					if(isset($_GET['codigo']))
					{
						require_once('resources/emogrifier/emogrifier.php');	
						$emogrifier = new \Pelago\Emogrifier();
						$html_limpio = php::get_page_code(get_permalink().'?limpio');
						$html_estilo = php::get_page_code(get_bloginfo('template_url').'/css/style-mail.css');
						$emogrifier->setHtml(preg_replace('/&(.*?);/','&amp;$1;',$html_limpio)); // Textarea Workaround
						$emogrifier->setCss($html_estilo);
						$html_final = $emogrifier->emogrify();
					}
					?>
					<textarea class="form-control textarea-no-resize-x borde-gris" rows="5" id="codigo-mail" name="codigo-mail" placeholder="Código Mail HTML"><?php echo $html_final; ?></textarea>
				</div>
			</div>
			<?php endif; ?>
			<div class="col-12">
				<?php if(isset($_GET['codigo'])): ?>
				<a href="#" class="btn btn-primary mr-1" data-clipboard-target="#codigo-mail" id="codigo-copiar">Copiar <i class="far fa-copy"></i></a>
				<a href="<?php echo get_permalink(); ?>/?codigo" class="btn btn-success">Volver a generar <i class="fas fa-sync"></i></a>
				<?php else: ?>
				<a href="<?php echo get_permalink(); ?>/?codigo" class="btn btn-success">Generar <i class="fas fa-sync"></i></a>
				<?php endif; ?>
			</div>
		</div>
		
		<div class="row">
			<div class="col-12 mt-4">
				<h2 class="border-bottom pb-3 mb-3">Previsualizaci&oacute;n</h2>
			</div>
		</div>
		
		<div class="row">
			<div class="col-12 mt-4">
				<div class="d-block overflow-auto text-center">
					<div class="codigo d-inline-block mail-css">
					<!-- Código -->
					<?php echo get_template_part('content', 'mail'); ?>
					<!-- Código -->
					<?php php::section('footer','start'); ?>
					<link href="<?php echo get_bloginfo('template_url'); ?>/css/style-mail.css" rel="stylesheet">
					<?php php::section('footer','end'); ?>
					</div>
				</div>
			</div>
		</div>
		
		<!-- MAIN CONTAINER -->
	</div>
</div>
<!-- ================================================= CONTENT ================================================= -->

<?php //endwhile; else: endif; ?>

<?php get_footer(); ?>

<?php endif; ?>