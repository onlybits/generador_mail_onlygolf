<?php
$mail_random = '?v='.rand(1000, 9000);
$mail_banner = get_field('mail_banner');
?>
<table class="header" cellpadding="0" cellspacing="0" border="0" align="center">
	<tbody>
		<tr>
			<td class="contfecha">
				<div class="box"><img class="logo" src="<?php echo get_bloginfo('template_url'); ?>/img/mail/logocachaguachico.jpg<?php echo $img; ?>"></div>
				<div class="tituloheader">Boletín<br>Informativo</div>
				<div class="fecha"><?php echo ucfirst(get_the_time('l')).' '.get_the_time('d').' de '.get_the_time('F').' '.get_the_time('Y'); ?></div>
				<img class="line" src="<?php echo get_bloginfo('template_url'); ?>/img/mail/line.png<?php echo $img; ?>">
			</td>
			<td>
				<?php if(!empty($mail_banner)): ?>
				<img src="<?php echo $mail_banner['url']; ?><?php echo $mail_random; ?>" alt="Club de Golf Cachagua">
				<?php else: ?>
				<img src="<?php echo get_bloginfo('template_url'); ?>/img/mail/fondocachagua.png<?php echo $img; ?>" alt="Club de Golf Cachagua">
				<?php endif; ?>
			</td>
		</tr>
	</tbody>
</table>
<table class="content" cellpadding="0" cellspacing="0" border="0" align="center">
<tbody>
		<?php 
		$mail_cantidad = count(get_field('mail_items'));
		$mail_unico = $mail_cantidad == 1 ? true : false;
		$mail_contador = 1;
		$mail_grupo = 0;
		while(have_rows('mail_items')): the_row();
		?>
		<?php
		$dato_titulo = get_sub_field('mail_item_titulo');
		$dato_texto = strip_tags(get_sub_field('mail_item_texto'), '<strong><b><i><u><a><ul><ol><p><li><br><img>');
		$dato_imagen = get_sub_field('mail_item_imagen');
		$dato_boton_link = get_sub_field('mail_item_boton_link');
		$dato_boton_texto = get_sub_field('mail_item_boton_texto');
		$mail_tipo = get_taxonomy_data('slug', 'tipo_mail');
		?>
		<?php ++$mail_grupo; if($mail_grupo == 1): ?>
		<tr class="<?php echo $mail_contador == 1 && $mail_cantidad > 4 ? 'primero' : ($mail_contador == $mail_cantidad || $mail_contador == ($mail_cantidad - 1) || $mail_contador == 1 ? 'ultimo' : ''); ?>">
		<?php endif; ?>
			<td class="<?php echo $mail_contador % 2 == 0 ? 'der' : 'izq'; ?> <?php echo $mail_unico ? 'unica' : ''; ?>" valign="top">
				<table>
					<?php if($mail_tipo === 'newsletter'): ?>
						
						<tr>
							<div class="<?php echo $mail_unico ? 'titulocomunicado' : 'titulo'; ?>"><?php echo $dato_titulo; ?></div>
						</tr>
						<tr>
							<?php if(!empty($dato_imagen)): ?>
							<td>
								<!-- Imagen -->
								<div class="<?php echo $mail_unico ? 'fotocomunicado' : 'foto'; ?>">
									<img src="<?php echo $dato_imagen['url']; ?><?php echo $mail_random; ?>">
								</div>
								<!-- Imagen -->
							</td>
							<?php endif; ?>
							<td>
								<div class="conttext">
									<!-- Texto -->
									<div class="texto"><?php echo $dato_texto; ?></div>
									<!-- Texto -->
								</div>
							<?php if(!empty($dato_boton_link)): ?>
								<!-- Boton -->
								<div class="conttext">
									<div class="boton">
										<a href="<?php echo $dato_boton_link; ?>" target="_blank" class="leer">
											<?php echo $dato_boton_texto; ?>
										</a>
									</div>
								</div>
								<!-- Boton -->
							<?php endif; ?>
							</td>
						</tr>
					
					<?php else: ?>
						<tr>
							<td>
								<div class="texto especial">Comunicado</div>
								<div class="conttextcomunicado">
									<div class="<?php echo $mail_unico ? 'titulocomunicado' : 'titulo'; ?>"><?php echo $dato_titulo; ?></div>
								</div>
							</td>
						</tr>
						<tr>
							
							<td>
								<?php if(!empty($dato_imagen)): ?>
									<!-- Imagen -->
									<div class="<?php echo $mail_unico ? 'fotocomunicado' : 'foto'; ?>">
										<img src="<?php echo $dato_imagen['url']; ?><?php echo $mail_random; ?>">
									</div>
									<!-- Imagen -->
								<?php endif; ?>
								
								<div class="conttextcomunicado">
									<!-- Texto -->
									<div class="texto"><?php echo $dato_texto; ?></div>
									<!-- Texto -->
								<?php if(!empty($dato_boton_link)): ?>
									<!-- Boton -->
									<div class="botoncomunicado">
										<a href="<?php echo $dato_boton_link; ?>" target="_blank" class="leer">
											<?php echo $dato_boton_texto; ?>
										</a>
									</div>
								<?php endif; ?>
								</div>
									<!-- Boton -->
								
							</td>
						</tr>
					<?php endif; ?>
				</table>
			</td>
		<?php if($mail_grupo == 2): ?>
		</tr>
		<?php $mail_grupo = 0; endif; ?>
		<?php
		$mail_contador++;
		endwhile;
		?>
	</tbody>
</table>
<table class="footer" cellpadding="0" cellspacing="0" border="0" align="center">
	<tbody>
		<tr>
			<td class="informacion">
				<div class="social">
					<table cellpadding="0" cellspacing="0" border="0" align="center">
						<tbody>
							<tr>
								<td align="left">
									<a href="http://www.clubdegolfcachagua.cl" target="_blank">www.clubdegolfcachagua.cl</a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="texto">
					<b>Club de Golf Cachagua</b><br>
					Camino el Golf 2021, Cachagua<br>
					Tel&eacute;fonos : +56 3 3277 1001 &#124; +56 3 3277 1595 - E-mail: <a href="mailto:cgc@clubdegolfcachagua.cl">cgc@clubdegolfcachagua.cl</a><br>
					Copyright (C) Club de Golf Cachagua  -  Todos los derechos reservados<br>
				</div>
			</td>
		</tr>
	</tbody>
</table>