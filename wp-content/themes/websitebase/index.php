<?php get_header(); ?>

<!-- ================================================= CONTENT ================================================= -->
<div class="content">
	<div class="container">
		<!-- MAIN CONTAINER -->
		
		<div class="row">
			<div class="col-12">
				<h2 class="border-bottom pb-3 mb-3">Listado mails</h2>
				<p>Listado completo de los mails creados para visualizarlo u obtener su <b>c&oacute;digo</b>.</p>
				<form id="formulario-home" novalidate method="post" action="<?php echo get_bloginfo('url'); ?>" class="row">
					<!-- Campos -->
					<div class="col-12 col-sm-9">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">
									<i class="align-self-center fas fa-envelope fa-lg"></i>
								</span>
							</div>
							<select class="custom-select" name="input-mail" required>
								<option value="" selected="true">Seleccione</option>
								<?php
								$my_taxonomy_query = array(
											'taxonomy'		=> 'tipo_mail',
											'order'			=> 'ASC',
											'orderby'		=> 'title',
											'show_count'	=> $show_count,
											'pad_counts'	=> $pad_counts,
											'hierarchical'	=> $hierarchical,
											'title_li'		=> $title,
											'hide_empty'	=> $empty
											);

								$my_taxonomy_list = get_categories($my_taxonomy_query); 

								foreach($my_taxonomy_list as $my_taxonomy): 
								$tax_slug = $my_taxonomy->slug;
								$tax_name = $my_taxonomy->name;
								$tax_desc = $my_taxonomy->description;
								$tax_field = get_field('custom_field_slug', $my_taxonomy);
								?>
								<optgroup label="<?php echo $tax_name; ?>">
									<?php 
									$my_query_args = array('post_type'	=> 'mail',
														   'tipo_mail'	=> $tax_slug,
														   'showposts'	=> -1);
									$my_query = new WP_Query($my_query_args);
									while($my_query->have_posts()) : $my_query->the_post(); $do_not_duplicate = $post->ID; 
									?>
									<option value="<?php echo get_the_slug(); ?>">&bull; <?php echo get_the_title(); ?></option>
									<?php 
									endwhile; 
									?>
								</optgroup>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="col-12 col-sm-3 mt-3 mt-sm-0">
						<div class="form-group mb-0">
							<button type="submit" class="btn btn-danger w-100" id="buscador-clubes-btn">Ver aqu&iacute; <i class="fas fa-arrow-right ml-1"></i></button>
						</div>
					</div>		
					<!-- Campos -->
				</form>
			</div>
		</div>
		
		<!-- MAIN CONTAINER -->
	</div>
</div>
<!-- ================================================= CONTENT ================================================= -->

<?php get_footer(); ?>