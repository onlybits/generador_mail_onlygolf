/* ================================================= THEME FUNCTIONS ================================================= */



/* ================================================= THEME FUNCTIONS ================================================= */

$(document).ready(function(){

/* ================================================= THEME DOCUMENT READY ================================================= */
	
	// Alineacion modal
	JShashTagAlignment = ['medium','center'];
	
	// Validar formulario
	$('#formulario-home').JSvalidateForm({
		noValidate: false,
		hasConfirm: false,
		resetSubmit: true,
		errorStyling: true,
		errorScroll: true,
		errorModal: true,
		modalSize: 'medium',
		modalAlign: 'center',
		modalAnimate: true,
		customValidate: false,
		customSubmit: function(){
			// Element
			var elem = $('#formulario-home');
			// Maun url
			var url = $(elem).attr('action');
			// Serialize form data
			var form_data = $(elem).serializeArray(); //.serialize();
			// Redirect
			window.location = url+'/mail/'+form_data[0].value;
		},
	});
	
	// Tooltip load
	$('#codigo-mail').tooltip({
		title: 'Código copiado!',
		placement: 'bottom',
		trigger: 'manual',
	});

	// Clipboard
	var clipboard = new ClipboardJS('#codigo-copiar');

	clipboard.on('success', function(e){
		// Show tooltip
		$('#codigo-mail').tooltip('show');

		// Disable click
		$(document).on('click', '#codigo-copiar', function(e){
			e.preventDefault();
		});
	});
	
	$('#codigo-copiar').click(function(){
		$('#codigo-mail').tooltip('show');
	});
	
	$('#codigo-mail').on('shown.bs.tooltip', function(){
		setTimeout(function(){
			// Hide tooltip
			$('#codigo-mail').tooltip('hide');
		}, 3000);
	})
	
/* ================================================= THEME DOCUMENT READY ================================================= */

});

$(window).bind('load', function(){

/* ================================================= THEME WINDOWS LOAD ================================================= */
	
	
	
/* ================================================= THEME WINDOWS LOAD ================================================= */

});

$(document).on('JSresponsiveCode', function(event, bodyWidth, bodyHeight, bodyOrientation, bodyScreen){

/* ================================================= THEME RESPONSIVE CODE ================================================= */
	
	
	
/* ================================================= THEME RESPONSIVE CODE ================================================= */

});

$(document).ajaxStart(function(){

/* ================================================= THEME AJAX START ================================================= */
	
	
	
/* ================================================= THEME AJAX START ================================================= */

});

$(document).ajaxComplete(function(){

/* ================================================= THEME AJAX COMPLETE ================================================= */
	
	
	
/* ================================================= THEME AJAX COMPLETE ================================================= */

});
