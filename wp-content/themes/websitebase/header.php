<!DOCTYPE html>
<html lang="<?php echo php::get_data('lang'); ?>">
<head>
	<title>Generador Mail <?php echo php::get_data('title'); ?><?php echo wp_title('&raquo;'); ?></title>
	
	<!-- ******** META TAGS ******** -->
	
	<!-- HTML Charset -->
	<meta charset="<?php echo php::get_data('charset'); ?>">
	<!-- Mobile Enable -->
	<meta name="mobile-web-app-capable" content="<?php echo php::get_data('mobile-capable'); ?>">
	<meta name="apple-mobile-web-app-capable" content="<?php echo php::get_data('mobile-capable'); ?>">
	<meta name="viewport" content="<?php echo php::get_data('viewport'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- Nav Bar Mobile Color -->
	<meta name="theme-color" content="<?php echo php::get_data('nav-color'); ?>">
	<meta name="msapplication-navbutton-color" content="<?php echo php::get_data('nav-color'); ?>">
	<meta name="apple-mobile-web-app-status-bar-style" content="<?php echo php::get_data('nav-color-apple'); ?>">
	<!-- Meta Details -->
	<meta name="description" content="<?php echo php::get_data('description'); ?>">
	<meta name="keywords" content="<?php echo php::get_data('keywords'); ?>">
	<meta name="author" content="<?php echo php::get_data('author'); ?>">
	
	<!-- ******** META TAGS ******** -->
	
	<!-- ******** HEADER RESOURCES ******** -->
	
	<!-- Favicon -->
	<link href="<?php echo get_bloginfo('template_url'); ?>/img/base/favicon/all.png" rel="icon">
	<!-- Apple Touch Icon -->
	<link href="<?php echo get_bloginfo('template_url'); ?>/img/base/favicon/apple.png" rel="apple-touch-icon">
	<!-- Bootstrap -->
	<link href="<?php echo get_bloginfo('template_url'); ?>/resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<!-- Data Tables Bootstrap -->
	<link href="<?php echo get_bloginfo('template_url'); ?>/resources/datatables/css/dataTables.bootstrap4.min.css" rel="stylesheet">
	<!-- Tempus Dominus -->
	<link href="<?php echo get_bloginfo('template_url'); ?>/resources/tempusdominus/css/tempusdominus-bootstrap-4.min.css" rel="stylesheet">
	<!-- jQuery UI -->
	<link href="<?php echo get_bloginfo('template_url'); ?>/resources/jquery-ui/css/jquery-ui.min.css" rel="stylesheet">
	<link href="<?php echo get_bloginfo('template_url'); ?>/resources/jquery-ui/css/jquery-ui.structure.min.css" rel="stylesheet">
	<link href="<?php echo get_bloginfo('template_url'); ?>/resources/jquery-ui/css/jquery-ui.theme.min.css" rel="stylesheet">
	<!-- LightGallery -->
	<link href="<?php echo get_bloginfo('template_url'); ?>/resources/lightgallery/css/lightgallery.min.css" rel="stylesheet">
	<link href="<?php echo get_bloginfo('template_url'); ?>/resources/lightgallery/css/lg-transitions.min.css" rel="stylesheet">
	<link href="<?php echo get_bloginfo('template_url'); ?>/resources/lightgallery/css/lg-fb-comment-box.min.css" rel="stylesheet">
    <!-- Hover CSS -->
	<link href="<?php echo get_bloginfo('template_url'); ?>/resources/hover/css/hover.min.css" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="<?php echo get_bloginfo('template_url'); ?>/resources/fontawesome/css/all.min.css" rel="stylesheet">
	<!-- Check Old Browser -->
	<script src="<?php echo get_bloginfo('template_url'); ?>/js/app-browser.js"></script>
	<!-- Main CSS File -->
	<?php echo php::get_template('css'); ?>
	
	<!-- ******** HEADER RESOURCES ******** -->
	
	<!-- Extra Code -->
	<?php echo php::section('header','get'); ?>
	
	<?php
	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
	?>
</head>
<body data-js-lang="es" data-js-hashtag="true" <?php echo is_home() ? 'data-js-home="true"' : ''; ?> data-js-console="false">
<!-- ================================================= ANALYTICS ================================================= -->
<?php if(php::is_localhost()): ?>
<script>
	function ga(){ console.log('Google Analytics:\n', arguments); } //Dont track in localhost
</script>
<?php else: ?>
<?php echo php::convert_to_utf8(get_option('bloganalytics'))."\n"; ?>
<?php endif; ?>
<!-- ================================================= ANALYTICS ================================================= -->

<!-- ================================================= HEADER ================================================= -->
<div class="header">
	<div class="container">
		<!-- HEADER CONTAINER -->
		
		<?php if(is_home()): ?>
		<div class="row">
			<div class="col-12 mt-4">
				<div class="jumbotron">
					<h1>Generador Mail <span class="badge badge-danger align-top"><?php echo get_bloginfo('title'); ?></span></h1>
					<p class="lead mb-0">
						Generador de mailings para utilizar con un gestor como <code>Mailchimp</code> o <code>Fidelizador</code>.
						<?php if(is_user_logged_in()): ?>
						Para ir al panel haga click <a href="<?php echo get_bloginfo('url'); ?>/wp-admin"><strong>aqu&iacute;</strong></a>
						<?php else: ?>
						Para conectarse haga click <a href="<?php echo get_bloginfo('url'); ?>/wp-admin"><strong>aqu&iacute;</strong></a>
						<?php endif; ?>
					</p>
				</div>
			</div>
		</div>
		<?php else: ?>
		<div class="row">
			<div class="col-12 mt-4">
				<div class="jumbotron py-5">
					<?php if(have_posts()) : while (have_posts()) : the_post(); ?>
					<h3><span class="badge badge-danger align-top"><?php echo get_taxonomy_data('name', 'tipo_mail'); ?></span> <?php echo get_the_title(); ?></h3>
					<p class="lead mb-0">
						Preparación de código en <code>HTML</code> para integrar en la plantilla del gestor de correos. Para volver al inicio haga <a href="<?php echo get_bloginfo('url'); ?>"><strong>aqu&iacute;</strong></a>
					</p>
					<?php endwhile; else: endif; ?>
				</div>
			</div>
		</div>
		<?php endif; ?>
    	
		<!-- HEADER CONTAINER -->
    </div>
</div>
<!-- ================================================= HEADER ================================================= -->
