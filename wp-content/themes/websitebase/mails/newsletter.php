<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html dir="ltr" xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" lang="es">
<head>
<meta charset="UTF-8">
<meta content="width=device-width, initial-scale=1" name="viewport">
<meta name="x-apple-disable-message-reformatting">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="telephone=no" name="format-detection">
<title>Newsletter Onlygolf 2023</title><!--[if (mso 16)]>
<style type="text/css">
a {text-decoration: none;}
</style>
<![endif]--><!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]--><!--[if gte mso 9]>
<xml>
<o:OfficeDocumentSettings>
<o:AllowPNG></o:AllowPNG>
<o:PixelsPerInch>96</o:PixelsPerInch>
</o:OfficeDocumentSettings>
</xml>
<![endif]-->
	<style type="text/css">
		#outlook a {
			padding:0;
		}
		.es-button {
			mso-style-priority:100!important;
			text-decoration:none!important;
		}
		a[x-apple-data-detectors] {
			color:inherit!important;
			text-decoration:none!important;
			font-size:inherit!important;
			font-family:inherit!important;
			font-weight:inherit!important;
			line-height:inherit!important;
		}
		.es-desk-hidden {
			display:none;
			float:left;
			overflow:hidden;
			width:0;
			max-height:0;
			line-height:0;
			mso-hide:all;
		}
		@media only screen and (max-width:600px){
			p, ul li, ol li, a { 
				line-height:150%!important; 
			} 
			h1, h2, h3, h1 a, h2 a, h3 a {
				line-height:120%; 
			}
			h1 { 
				font-size:30px!important;
				text-align:left; 
			} 
			h2 {
				font-size:24px!important;
				text-align:left;
			} 
			h3 { 
				font-size:20px!important; 
				text-align:left; 
			} 
			.es-header-body h1 a,
			.es-content-body h1 a,
			.es-footer-body h1 a {
				font-size:30px!important;
				text-align:left; 
			} 
			.es-header-body h2 a,
			.es-content-body h2 a,
			.es-footer-body h2 a {
				font-size:24px!important;
				text-align:left; 
			} 
			.es-header-body h3 a,
			.es-content-body h3 a,
			.es-footer-body h3 a {
				font-size:20px!important;
				text-align:left; 
			} 
			.es-menu td a {
				font-size:13px!important; 
			} 
			.es-header-body p,
			.es-header-body ul li,
			.es-header-body ol li,
			.es-header-body a {
				font-size:14px!important;
			} 
			.es-content-body p,
			.es-content-body ul li,
			.es-content-body ol li,
			.es-content-body a {
				font-size:13px!important;
			} 
			.es-footer-body p,
			.es-footer-body ul li,
			.es-footer-body ol li, 
			.es-footer-body a { 
				font-size:14px!important; 
			} 
			.es-infoblock p, 
			.es-infoblock ul li, 
			.es-infoblock ol li, 
			.es-infoblock a { 
				font-size:12px!important; 
			} 
			*[class="gmail-fix"] { 
				display:none!important; 
			} 
			.es-m-txt-c,
			.es-m-txt-c h1, 
			.es-m-txt-c h2,
			.es-m-txt-c h3 { 
				text-align:center!important; 
			} 
			.es-m-txt-r, 
			.es-m-txt-r h1, 
			.es-m-txt-r h2,
			.es-m-txt-r h3 {
				text-align:right!important;
			} 
			.es-m-txt-l, 
			.es-m-txt-l h1,
			.es-m-txt-l h2,
			.es-m-txt-l h3 {
				text-align:left!important;
			} 
			.es-m-txt-r img, 
			.es-m-txt-c img, 
			.es-m-txt-l img { 
				display:inline!important; 
			} 
			.es-button-border {
				display:inline-block!important; 
			} 
			a.es-button, 
			button.es-button {
				font-size:11px!important; 
				display:inline-block!important; 
			} 
			.es-adaptive table, 
			.es-left, 
			.es-right {
				width:100%!important; 
			} 
			.es-content table, 
			.es-header table, 
			.es-footer table,
			.es-content, 
			.es-footer, 
			.es-header {
				width:100%!important; 
				max-width:600px!important; 
			} 
			.es-adapt-td { 
				display:block!important;
				width:100%!important;
			} 
			.adapt-img {
				width:100%!important;
				height:auto!important; 
			} 
			.es-m-p0 { 
				padding:0!important;
			} 
			.es-m-p0r {
				padding-right:0!important; 
			} 
			.es-m-p0l {
				padding-left:0!important; 
			} 
			.es-m-p0t {
				padding-top:0!important;
			} 
			.es-m-p0b {
				padding-bottom:0!important;
			} 
			.es-m-p20b {
				padding-bottom:20px!important; 
			}
			.es-mobile-hidden,
			.es-hidden { 
				display:none!important;
			} 
			tr.es-desk-hidden, 
			td.es-desk-hidden,
			table.es-desk-hidden { 
				width:auto!important; 
				overflow:visible!important;
				float:none!important;
				max-height:inherit!important;
				line-height:inherit!important; 
			} 
			tr.es-desk-hidden { 
				display:table-row!important;
			} 
			table.es-desk-hidden {
				display:table!important;
			} 
			td.es-desk-menu-hidden { 
				display:table-cell!important;
			} 
			.es-menu td {
				width:1%!important;
			}
			table.es-table-not-adapt, 
			.esd-block-html table {
				width:auto!important; 
			} 
			table.es-social { 
				display:inline-block!important; 
			} 
			table.es-social td {
				display:inline-block!important; 
			} 
			.es-m-p5 {
				padding:5px!important; 
			} 
			.es-m-p5t { 
				padding-top:5px!important;
			}
			.es-m-p5b { 
				padding-bottom:5px!important;
			} 
			.es-m-p5r { 
				padding-right:5px!important;
			}
			.es-m-p5l { 
				padding-left:5px!important;
			}
			.es-m-p10 { 
				padding:10px!important; 
			}
			.es-m-p10t { 
				padding-top:10px!important;
			} 
			.es-m-p10b { 
				padding-bottom:10px!important; 
			} 
			.es-m-p10r { 
				padding-right:10px!important;
			} 
			.es-m-p10l {
				padding-left:10px!important; 
			}
			.es-m-p15 { 
				padding:15px!important;
			} 
			.es-m-p15t {
				padding-top:15px!important;
			}
			.es-m-p15b {
				padding-bottom:15px!important;
			}
			.es-m-p15r {
				padding-right:15px!important; 
			} 
			.es-m-p15l { 
				padding-left:15px!important;
			} 
			.es-m-p20 {
				padding:20px!important;
			} 
			.es-m-p20t { 
				padding-top:20px!important; 
			}
			.es-m-p20r { 
				padding-right:20px!important; 
			}
			.es-m-p20l { 
				padding-left:20px!important; 
			}
			.es-m-p25 {
				padding:25px!important; 
			} 
			.es-m-p25t { 
				padding-top:25px!important; 
			}
			.es-m-p25b { 
				padding-bottom:25px!important;
			} 
			.es-m-p25r { 
				padding-right:25px!important; 
			}
			.es-m-p25l {
				padding-left:25px!important; 
			} 
			.es-m-p30 { 
				padding:30px!important;
			} 
			.es-m-p30t {
				padding-top:30px!important; 
			} 
			.es-m-p30b { 
				padding-bottom:30px!important; 
			}
			.es-m-p30r { 
				padding-right:30px!important;
			} 
			.es-m-p30l { 
				padding-left:30px!important;
			} 
			.es-m-p35 { 
				padding:35px!important;
			} 
			.es-m-p35t { 
				padding-top:35px!important;
			}
			.es-m-p35b { 
				padding-bottom:35px!important;
			}
			.es-m-p35r { 
				padding-right:35px!important;
			} 
			.es-m-p35l {
				padding-left:35px!important;
			} 
			.es-m-p40 { 
				padding:40px!important; 
			} 
			.es-m-p40t { 
				padding-top:40px!important; 
			}
			.es-m-p40b { 
				padding-bottom:40px!important;
			} 
			.es-m-p40r { 
				padding-right:40px!important;
			} 
			.es-m-p40l { 
				padding-left:40px!important;
			} 
			.es-desk-hidden { 
				display:table-row!important;
				width:auto!important; 
				overflow:visible!important; 
				max-height:inherit!important; 
			} 
		}
	</style>
</head>
	
<body style="width:100%;font-family:arial, 'helvetica neue', helvetica, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0">
<div dir="ltr" class="es-wrapper-color" lang="es" style="background-color:#FFFFFF"><!--[if gte mso 9]>
<v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
<v:fill type="tile" color="#ffffff"></v:fill>
</v:background>
<![endif]-->
<table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top;background-color:#FFFFFF">
<tr>
<td valign="top" style="padding:0;Margin:0">
<table class="es-content" cellspacing="0" cellpadding="0" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%">
<tr>
<td align="center" style="padding:0;Margin:0">
<table class="es-content-body" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:590px">
<tr>
<td align="left" style="padding:0;Margin:0">
<table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
<tr>
<td valign="top" align="center" style="padding:0;Margin:0;width:590px">
<table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
<tr>
<td align="center" style="padding:0;Margin:0;font-size:0px"><img class="adapt-img" src="<?php echo get_bloginfo('template_url'); ?>/img/bannernews.png" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" width="590"></td>
</tr>
</table></td>
</tr>
</table></td>
</tr>
<tr>
<td align="left" style="padding:0;Margin:0">
<table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
<tr>
<td align="center" valign="top" style="padding:0;Margin:0;width:590px">
<table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
<tr>
<td class="es-m-txt-c es-m-p20t es-m-p0r" align="center" style="padding:0;Margin:0;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:24px;color:#666666;font-size:16px"><strong><font style="vertical-align:inherit"><font style="vertical-align:inherit"><?php echo ucfirst(get_the_time('l')); ?> <span style="color:#0074b9"><?php echo ucfirst(get_the_time('d')).' de '.get_the_time('F').', '.get_the_time('Y'); ?></span></font></font>&nbsp;</strong></p></td>
</tr>
</table></td>
</tr>
</table></td>
</tr>
</table></td>
</tr>
</table>
	
  <?php
		$mail_cantidad = count(get_field('mail_items'));
		$mail_unico = $mail_cantidad == 1 ? true :  false;
		$mail_contador = 1;
		$mail_grupo = 0;

		while(have_rows('mail_items')):  the_row();

		$dato_titulo = get_sub_field('mail_item_titulo');
		$dato_imagen = get_sub_field('mail_item_imagen');
		$dato_boton_link = get_sub_field('mail_item_boton_link');
		$dato_boton_texto = get_sub_field('mail_item_boton_texto');
		$mail_tipo = get_taxonomy_data('slug', 'tipo_mail');

	  if(get_row_index()%2 != 0): ?>	
	
		<table cellpadding="0" cellspacing="0" class="es-content" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%">
			<tr>
				<td align="center" style="padding:0;Margin:0">
					<table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:590px">
						<tr>
							<td align="left" style="padding:0;Margin:0">
								<table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
									<tr>
										<td align="center" valign="top" style="padding:0;Margin:0;width:590px">
											<table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
												<tr>
													<td align="center" class="es-m-p10r es-m-p10l" style="padding:0;Margin:0;padding-top:20px;padding-left:25px;padding-right:25px;font-size:0px" bgcolor="#ffffff"><img class="adapt-img" src="<?php echo $dato_imagen['url']; ?>" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" width="540">
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td class="es-m-p10r es-m-p10l" align="left" bgcolor="#ffffff" style="Margin:0;padding-top:10px;padding-bottom:20px;padding-left:25px;padding-right:25px">
								<table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
									<tr>
										<td align="center" valign="top" style="padding:0;Margin:0;width:540px">
											<table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
												<tr>
													<td align="left" style="padding:0;Margin:0"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:23px;color:#666666;font-size:15px;text-align:center;"><strong><?php echo $dato_titulo; ?></strong></p>
													</td>
												</tr>
											<tr>
												<td align="center" style="padding:0;Margin:0;padding-top:10px">
													<?php if ($dato_boton_link): ?>
													<span class="es-button-border" style="border-style:solid;border-color:#2cb543;background:#ffffff;border-width:0px;display:inline-block;border-radius:0px;width:auto">
														<a href="<?php echo $dato_boton_link; ?>" class="es-button es-button-1697216076255" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;color:#ffffff;font-size:11px;display:inline-block;background:#0074b9;border-radius:0px;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-weight:bold;font-style:normal;line-height:13px;width:auto;text-align:center;padding:6px 35px;mso-padding-alt:0;mso-border-alt:10px solid #31CB4B;text-transform: uppercase;"><?php echo $dato_boton_texto; ?></a>
													</span>
													<?php endif ?>
												</td>
											</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>

	<?php else: ?>
		<table cellpadding="0" cellspacing="0" class="es-content" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%">
			<tr>
				<td align="center" style="padding:0;Margin:0">
					<table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:590px">
						<tr>
							<td align="left" style="padding:0;Margin:0">
								<table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
									<tr>
										<td align="center" valign="top" style="padding:0;Margin:0;width:590px">
											<table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
												<tr>
													<td align="center" class="es-m-p10r es-m-p10l" style="padding:0;Margin:0;padding-top:20px;padding-left:25px;padding-right:25px;font-size:0px" bgcolor="#efefef">
														<img class="adapt-img" src="<?php echo $dato_imagen['url']; ?>" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" width="540">
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td class="es-m-p10r es-m-p10l" align="left" bgcolor="#efefef" style="Margin:0;padding-top:10px;padding-bottom:20px;padding-left:25px;padding-right:25px">
								<table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
									<tr>
										<td align="center" valign="top" style="padding:0;Margin:0;width:540px">
											<table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
												<tr>
													<td align="left" style="padding:0;Margin:0">
														<p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:23px;color:#666666;font-size:15px;text-align:center;"><strong><?php echo $dato_titulo; ?></strong></p>
													</td>
												</tr>
												<tr>
													<td align="center" style="padding:0;Margin:0;padding-top:10px">
														<?php if ($dato_boton_link): ?>
														<span class="es-button-border" style="border-style:solid;border-color:#0074b9;background:#0074b9;border-width:0px;display:inline-block;border-radius:0px;width:auto">
															<a href="<?php echo $dato_boton_link; ?>" class="es-button es-button-1697149390612" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;color:#ffffff;font-size:11px;display:inline-block;background:#0074b9;border-radius:0px;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-weight:bold;font-style:normal;line-height:13px;width:auto;text-align:center;padding:6px 35px;mso-padding-alt:0;mso-border-alt:10px solid #0074b9;text-transform: uppercase;"><?php echo $dato_boton_texto; ?></a>
														</span>
														<?php endif ?>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
 	<?php endif; ?>
<?php endwhile; ?>
	
<!--AUSPICIADOR-->	
<table class="es-footer" cellspacing="0" cellpadding="0" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top">
	<tr>
		<td align="center" style="padding:0;Margin:0">
			<table class="es-footer-body" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:590px">
				<tr>
					<td align="left" bgcolor="#0074b9" style="padding:0;Margin:0;padding-bottom:15px;padding-left:20px;padding-right:0px;background-color:transparent">
						<table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
							<tr>
								<td align="center" valign="top" style="padding:0;Margin:0;width:550px">
									<table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
										<tr>
											<td align="center" class="es-m-p40r es-m-p25l" style="padding:0;Margin:0;padding-bottom:0px;padding-right:25px;font-size:0px">
												<a target="_blank" href="https://www.underarmour.cl/" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:underline;color:#FFFFFF;font-size:14px"><img src="<?php echo get_bloginfo('template_url'); ?>/img/auspiciador1.png" width="93" height="70"></a>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>	
</table>
<!--AUSPICIADOR-->	
	
<table class="es-footer" cellspacing="0" cellpadding="0" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top">
	<tr>
		<td align="center" style="padding:0;Margin:0">
			<table class="es-footer-body" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:590px">
				<tr>
					<td align="left" bgcolor="#0074b9" style="padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px;background-color:#0066a7">
						<table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
							<tr>
								<td align="center" valign="top" style="padding:0;Margin:0;width:550px">
									<table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
										<tr>
											<td align="center" class="es-m-p40r es-m-p25l" style="padding:0;Margin:0;padding-bottom:5px;padding-right:25px;font-size:0px"><a target="_blank" href="https://onlygolf.cl/" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:underline;color:#FFFFFF;font-size:14px"><img src="https://jhtjol.stripocdn.email/content/guids/CABINET_120608bdeb2f3b04b6a5fe14d32bb95d/images/logoonlygolfwhite.png" alt style="display:inline-block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" width="160" height="47"></a></td>
											</tr>
											<tr>
											<td align="center" style="padding:0;Margin:0"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#ffffff;font-size:14px">Síguenos por todos <strong>nuestras redes</strong></p>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center" bgcolor="#0074b9" style="Margin:0;padding-top:5px;padding-bottom:20px;padding-left:40px;padding-right:40px;background-color:#0066a7">
						<table cellpadding="0" cellspacing="0" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
							<tr>
								<td class="es-m-p0r" align="center" style="padding:0;Margin:0">
									<table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
										<tr>
											<td style="padding:0;Margin:0;font-size:0px"><a target="_blank" href="https://web.facebook.com/onlygolfchile/" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:underline;color:#FFFFFF;font-size:14px"><img src="https://jhtjol.stripocdn.email/content/guids/CABINET_120608bdeb2f3b04b6a5fe14d32bb95d/images/socialfacebookwhite.png" alt style="display:inline-block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;padding:0px 3px" width="35"></a></td>
											<td style="padding:0;Margin:0;font-size:0px"><a target="_blank" href="https://www.instagram.com/onlygolfcl/" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:underline;color:#FFFFFF;font-size:14px"><img src="https://jhtjol.stripocdn.email/content/guids/CABINET_120608bdeb2f3b04b6a5fe14d32bb95d/images/socialinstagramwhite.png" alt style="display:inline-block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;padding:0px 3px" width="35"></a></td>
											<td style="padding:0;Margin:0;font-size:0px"><a target="_blank" href="https://twitter.com/onlygolfcl" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:underline;color:#FFFFFF;font-size:14px"><img src="https://jhtjol.stripocdn.email/content/guids/CABINET_120608bdeb2f3b04b6a5fe14d32bb95d/images/socialtwitterwhite.png" alt style="display:inline-block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;padding:0px 3px" width="35"></a></td>
											<td style="padding:0;Margin:0;font-size:0px"><a target="_blank" href="https://www.youtube.com/channel/UCNpzEgTaHhw9wcv-dXaB3tQ" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:underline;color:#FFFFFF;font-size:14px"><img src="https://jhtjol.stripocdn.email/content/guids/CABINET_120608bdeb2f3b04b6a5fe14d32bb95d/images/socialyoutubewhite.png" alt style="display:inline-block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;padding:0px 3px" width="35"></a></td>
										</tr>
									</table>
								</td>
								<td class="es-hidden" style="padding:0;Margin:0;width:10px"></td>
							</tr>
						</table><!--[if mso]></td></tr></table><![endif]-->
					</td>
				</tr>
				<tr>
					<td style="padding:20px;Margin:0;background-color:#333333" bgcolor="#333" align="left">
						<table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
							<tr>
								<td valign="top" align="center" style="padding:0;Margin:0;width:550px">
									<table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
										<tr>
											<td align="center" style="padding:0;Margin:0"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#cccccc;font-size:14px">Teléfonos : <strong><a target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:none;color:#cccccc;font-size:14px" href="tel:+56983606964">+56&nbsp;9 8360 6964</a></strong>&nbsp;&nbsp;|&nbsp;&nbsp;<a target="_blank" href="mailto:info@onlygolf.cl" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:none;color:#cccccc;font-size:14px">Email: <strong>info@onlygolf.cl</strong></a><br>Vitacura, Santiago, Chile</p></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
	
</td>
</tr>
</table>
</div>
</body>
</html>