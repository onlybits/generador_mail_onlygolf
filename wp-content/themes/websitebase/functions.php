<?php

/*
 * PHP Main Resources
 * TriForce - Matías Silva
 *
 * This file calls the main PHP utilities and sets the main data (html and rebuild pass)
 * Don't add functions here, You can add your own in functions.php
 * 
 */

// Get the main PHP utilities
require_once('resources/utilities.php');
require_once('resources/wordpress.php');

// Enable main PHP utilities
class php extends utilities\php { }

// Set main Website Base data fields
$websitebase = array(
	'debug'				=> false,
	'lang' 				=> get_bloginfo('language'),
	'charset' 			=> get_option('blog_charset'),
	'title' 			=> get_option('blogname'),
	'description' 		=> get_option('blogdescription'),
	'keywords' 			=> get_option('blogkeywords'),
	'author' 			=> get_option('blogauthor'),
	'mobile-capable' 	=> 'yes',
	'viewport' 			=> 'width=device-width, initial-scale=1, user-scalable=no, shrink-to-fit=no',
	'nav-color' 		=> get_option('blognavcolor'),
	'nav-color-apple' 	=> get_option('blognavcolorapple'),
	'timezone' 			=> wp_get_timezone_string(), // 'America/New_York',
	'local_dir'			=> dirname(__FILE__),
	'custom_main_url'	=> false,
	'assets_url'		=> get_bloginfo('template_url'),
	'rebuild_pass'		=> 'generador2019',
	'minify'			=> true,
	'mix'				=> true,
	'css_file'			=> array(/*'css/extras/example.css',*/
								 /*'css/extras/example-2.css',*/
								 /*'css/extras/example-3.css',*/),
	'css_vars'			=> array('$color-gris'	=> '#ced4da',
								 /*'$color-custom-2'	=> '#FFFFFF',*/
								 /*'$color-custom-3'	=> '#FFFFFF',*/),
	'js_file'			=> array(/*'js/extras/example.js',*/
								 /*'js/extras/example-2.js',*/
								 /*'js/extras/example-3.js',*/),
	'js_vars'			=> array(/*'$color-custom'	=> '#FF0000',*/
								 /*'$color-custom-2'	=> '#FFFFFF',*/
								 /*'$color-custom-3'	=> '#FFFFFF',*/),
);

// Set default timezone
date_default_timezone_set($websitebase['timezone']);

// Rebuild CSS & JS redirect clean
php::check_rebuild();

// Check error warnings
php::debug();

/*
 * Wordpress Main Stuff
 * 
 * You can get some functions snippets in the URL below
 * https://github.com/TriForceX/WebsiteBase/wiki/Wordpress-Utilities#function-snippets
 * 
 */

// Disable updates notifications
function remove_core_updates()
{
	global $wp_version;
	return(object) array(
		'last_checked'=> time(),
		'version_checked'=> $wp_version,
	);
}
add_filter('pre_site_transient_update_core','remove_core_updates');
add_filter('pre_site_transient_update_plugins','remove_core_updates');
add_filter('pre_site_transient_update_themes','remove_core_updates');

// Remove dashboard widgets
function remove_dashboard_widgets() 
{
	// General
	remove_action( 'welcome_panel', 'wp_welcome_panel' );
	remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );   // Right Now
	remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' ); // Recent Comments
	remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );  // Incoming Links
	remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );   // Plugins
	remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );  // Quick Press
	remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );  // Recent Drafts
	remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );   // WordPress blog
	remove_meta_box( 'dashboard_secondary', 'dashboard', 'side' );   // Other WordPress News
	remove_meta_box( 'dashboard_activity', 'dashboard', 'normal' ); // Activity
	
	// Example by user role: Remove 'Simple History' Plugin widget
	if(!current_user_can('administrator'))
	{
		remove_meta_box('simple_history_dashboard_widget', 'dashboard', 'normal'); 
	}
	
	// Example by user role: Remove 'Simple History' Plugin widget
	if($user && isset($user->user_login) && 'user' == $user->user_login)
	{
		remove_meta_box('simple_history_dashboard_widget', 'dashboard', 'normal'); 
	}
}
add_action('wp_dashboard_setup', 'remove_dashboard_widgets');

// Custom CSS & JS footer
function admin_inline()
{
	if(!current_user_can('administrator')):
	?>
		<style type="text/css">
			#wpadminbar #wp-admin-bar-wp-logo,
			#wpadminbar #wp-admin-bar-archive,
			#wpadminbar #wp-admin-bar-comments,
			#wpadminbar #wp-admin-bar-new-content,
			#wpadminbar #wp-admin-bar-search,
			#adminmenu #menu-users a[href="profile.php"],
			#wp-admin-bar-user-actions #wp-admin-bar-user-info,
			#wp-admin-bar-user-actions #wp-admin-bar-edit-profile,
			#screen-meta,
			#screen-meta-links,
			#side-sortables .inside .wp-hidden-children {
				display: none !important;
			}
			#wp-admin-bar-user-actions #wp-admin-bar-logout {
				margin-left: 0px !important;
				margin-right: 0px !important;
			}
			#wp-admin-bar-user-actions #wp-admin-bar-logout .ab-item {
				min-width: inherit !important;
				text-align: center !important;
			}
		</style>
		<script type="text/javascript">
			if(typeof $ !== 'function')
			{
				var $ = jQuery;
			}

			$(document).ready(function(){
				
				$('#wpadminbar #wp-admin-bar-wp-logo').remove();
				$('#wpadminbar #wp-admin-bar-archive').remove();
				$('#wpadminbar #wp-admin-bar-comments').remove();
				$('#wpadminbar #wp-admin-bar-new-content').remove();
				$('#wpadminbar #wp-admin-bar-search').remove();
				$('#adminmenu #menu-users a[href="profile.php"]').parents('#menu-users').remove();
				$('#wpadminbar #wp-admin-bar-my-account > .ab-item').attr('href','#');
				$('#wp-admin-bar-user-actions #wp-admin-bar-user-info').remove();
				$('#wp-admin-bar-user-actions #wp-admin-bar-edit-profile').remove();
				$('#screen-meta').remove();
				$('#screen-meta-links').remove();
				$('#side-sortables .inside .wp-hidden-children').remove();

			});
		</script>
	<?php
	endif;
}

add_action('admin_footer', 'admin_inline');
add_action('login_footer', 'admin_inline');
add_action('wp_footer', 'admin_inline');

if(is_user_logged_in())
{
	add_action('wp_footer', 'admin_inline');
}

// Media size limit
function filter_site_upload_size_limit( $size ) 
{
    // Set the upload size limit to 60 MB for users lacking the 'manage_options' capability.
    if (!current_user_can( 'manage_options')) 
	{
		$limit = 1; // in MB
		$size = (1 * 1024) * 1024;
    }
    return $size;
}
add_filter('upload_size_limit', 'filter_site_upload_size_limit', 20);
