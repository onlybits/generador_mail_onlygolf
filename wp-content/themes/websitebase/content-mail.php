<?php if (get_taxonomy_data('slug', 'tipo_mail') === 'newsletter'): ?>
	<?php include 'mails/newsletter.php'; ?>
<?php else: ?>
	<?php include 'mails/comunicado.php'; ?>
<?php endif ?>